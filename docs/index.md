# Home

![](./images/Cover.jpg)

<font size="5"> <b> Welcome to my Fundamentals on Digital Fabrication - Documentation site </b> </font>

This site is to document my activities during the _Fundamentals of digital fabrication_ course, offered by the [FabLab Kamp-Lintfort](https://fablab.hochschule-rhein-waal.de/) at the Hochschule Rhein Waal and executed by Mr. Daniele Ingrassia during the Winter Semester 2020/21

I'm taking this course because I want to improve my digital fabrication skills.

## About FabLab

[FabLab](https://es.wikipedia.org/wiki/Fab_lab) stands for Fabrication Laboratory, it is a digital manufacturing workshop for personal use that is a space for the production of physical objects on a personal or local scale that groups together machines controlled by computers. Its particularity lies in its size and in its strong link with society rather than with industry.

## About the FODF course

It is intended as a general overview and hands-on experience on nowadays available digital fabrication tools and techniques

This weekly course is composed of the next lectures, all of them with a practical assignment:

\- [Project management](https://jeffjosu.gitlab.io/JeffPage/assignments/week01/).  
\- [2D and 3D Design](https://jeffjosu.gitlab.io/JeffPage/assignments/week02/).  
\- [Laser Cutting](https://jeffjosu.gitlab.io/JeffPage/assignments/week03/).  
\- [3D Printing](https://jeffjosu.gitlab.io/JeffPage/assignments/week04/).  
\- [Embedded programming](https://jeffjosu.gitlab.io/JeffPage/assignments/week05/).  
\- [Input devices](https://jeffjosu.gitlab.io/JeffPage/assignments/week06/).  
\- [Output devices](https://jeffjosu.gitlab.io/JeffPage/assignments/week07/).  
\- [Electronics design](https://jeffjosu.gitlab.io/JeffPage/assignments/week08/).  

It finishes with a [Final Project](https://jeffjosu.gitlab.io/JeffPage/Project/ww01Presentation/) which contains aspects from all or most of the lectures.