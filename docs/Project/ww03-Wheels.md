# - Wheels | 3D design & 3D print

## Softwares and hardwares

\- For designing I used [Fusion 360](https://www.autodesk.com/education/edu-software/overview?sorting=featured&page=1#).  
\- For slicing I used [Cura](https://ultimaker.com/software).  
\- The 3D printer I used was a [Ultimaker S5](https://ultimaker.com/3d-printers/ultimaker-s5), with a AA 0.4 nozzle.  
\- The material I used for printing was generic PLA.

## 3D design

I designed it in three parts:  
\- Frame.  
\- Roller.  
\- Motor coupling.

I used parameters for the tolerances and diameter of the hole for the roller shaft.

![OmniWheelDesign](../images/FinalProject/OmniWheelPre.jpg)

![OmniWheelDesignAssembly](../images/FinalProject/OmniWheel.jpg)

<div style="position: relative; padding-bottom: 65%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
    <iframe title="A 3D model" width="640" height="480" src="https://sketchfab.com/models/95649838cb874e3894b83b1cf4c7897c/embed?autospin=0.2&amp;autostart=1&amp;ui_controls=1&amp;ui_infos=1&amp;ui_inspector=1&amp;ui_stop=1&amp;ui_watermark=1&amp;ui_watermark_link=1" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
</div>
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/3d-models/omniwheel-95649838cb874e3894b83b1cf4c7897c?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">OmniWheel</a>
    by <a href="https://sketchfab.com/jeffjosu?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Jeff Josue</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>

## 3D print

### Parameters

I used the following parameters for all of the printed pieces:

\- _Quality_ > _Layer Height_: 0.15mm (it makes my piece to look detailed enough and also some vertical parts don't loose their shape).

\- _Shell_ > _Wall thickness:_ 1mm (by default, I don't need it to be stronger than that).

\- _Infill_ > _Infill density:_ 25% (it's enough to the strength I need).

\- _Build Plate adhesion:_ Brim at 5mm (I only needed to make sure my pieces doesn't come out in the process).

\- _Support:_ Activated, at 20% (I activated it for the Frame).

> Further explanation about parameters, check [3D printing Parameters](https://jeffjosu.gitlab.io/JeffPage/assignments/week04/#parameters).

### Test piece

#### Why and How?

When printing parts for a mechanism, there are two tolerances to take care of:  
\- _Mechanical tolerance:_ due to the joints which can be fixed or with certain type of movement.  
\- _Printing tolerance:_ when the extruded molten plastic is squashed by the nozzle, it flows to the sides, giving slight dimensional errors (compared to their 3D designs).

There are different ways to compensate it:  
\- Using a parameter named "Horizontal expansion" in Ultimaker Cura.  
\- Changing the size of the piece in the slicer.  
\- Using parametric design and changing the measurements from the 3D design.

I chose the last one, to get the correction I need, I made a test piece with different offsets.

#### Making

- Design:  <br>
  ![TestSketch](../images/FinalProject/TestSketch.jpg)  
  ![Test3Ddesign](../images/FinalProject/OffsetTest.jpg)

  > I rounded the corners of the 'male' part to avoid possible problems such as extra material on it; since a nozzle is circular, we can't get sharp corners in the vertical axis.

- 3d Printing:  <br>
  ![PrintedTest](../images/FinalProject/PrintedTest.jpg)

#### Results

\- From 0.0mm to +0.10mm was impossible to introduce the male.

\- With +0.15mm and +0.20mm it was introducible, but some force was needed and it could break the male.

\- The +0.25mm resulted appropriate for fixed/rigid joints.
I applied it in the frame holes that hold the rollers shaft, in the Motor coupling hole, and between the Motor coupling and the Frame.

\- The +0.30mm is tight enough for been applied in fixed joints, but also loose enough for a slicer joint.

\- The +0.35mm resulted, for my taste, appropriate for cylindrical and revolute joints; even though for it I would also test some value between +0.30mm and +0.35mm.  
I applied it in the rollers hole for their shaft, and between the Rollers and the Frame.

> In this test, the offsets were applied only in one if the parts (the 'female' one), if you want to apply it to both of the pieces, just need to divide the value by 2.

### Wheels

#### Printing

- Frame:
    - Slicer:  <br>
      ![FrameCura](../images/FinalProject/FrameCura.jpg)  
    - Printed:  <br>
      ![PrintedFrame](../images/FinalProject/PrintedFrame.jpg)

- Rollers:  
    - Slicer:  <br>
      ![RollersCura](../images/FinalProject/RollersCura.jpg)
    - Printed:  <br>
      ![PrintedRollers](../images/FinalProject/PrintedRollers.jpg)

- Motor coupling:
    - Slicer:  <br>
      ![MotorShaftCura](../images/FinalProject/MotorShaftCura.jpg)
    - Printed:  <br>
      ![PrintedMotorShaft](../images/FinalProject/PrintedMotorShaft.jpg)


> Further explanation about the 3D printing process, check [3D printing Process](https://jeffjosu.gitlab.io/JeffPage/assignments/week04/#printing-process).

#### Assembling

As a shaft for the rollers I used 1-inch nails:  <br>
  ![PrintedOmni1](../images/FinalProject/PrintedOmni1.jpg)
  ![PrintedOmni2](../images/FinalProject/PrintedOmni2.jpg)
  ![PrintedOmni3](../images/FinalProject/PrintedOmni3.jpg)

## Download files

\- 3D design - Test piece: [TestPiece.f3d](../images/FinalProject/Tester.f3d)  
\- 3D design - OmniWheel: [OmniWheel.f3d](../images/FinalProject/OmniWheel.f3d)
