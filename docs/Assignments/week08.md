# Electronic production

This eighth week I worked on getting used to Electronic production.

Since due to Covid restrictions, it wasn't possible to go to the Lab and make the PCB, then we are just doing the designing part for learning, so I made a simple board with four I/O pins. 

## Software

\- For this practice we used [EAGLE](https://www.autodesk.com/products/eagle/free-download).

## Electronic design

This process is divided into two parts:  
1. Schematics, which is an electronic diagram editor where the Components can be placed with a single click and easily routable with other components based on cables or labels.  
2. Board, which is a PCB editor with a fairly efficient auto-router.  

### Schematic

After opening EAGLE:

- First we create an empty project: _Projects_ > right click on _projects_ > _New Project_ > type desired name > press _Enter_.  
- Create new Schematic: right click on the created project > _New_ > _Schematic_.  
- We download and install the _fab_ and _FAB Hello_ libraries to have the components we need (and also the ones they normally have at the FabLab when it comes to the real PCB production). Once we downloaded the libraries: Click on _Library_ tab > _Open library manager_ > _In use_ > _Browse..._ > select the downloaded libraries > _Open_.  
- We can start adding components: _Add part_ > search the component we need in the libraries or if we know the name, can just type it:  
    ![Schematic](../images/week08/Schematic2.jpg)  
- Once we added all of the components we need, we can start wiring. It's also recommendable to use labels rather then cables to make clearer and easier to edit later:  
    ![Schematic](../images/week08/Schematic.jpg)  
    - I used an Attiny44-SSU, basic components for reset and SPI connection, 4 I/O pins, 2 Power pins; all of it supported on the datasheet of the microcontroller.  
- Once the Schematic is ready, we can start designing the PCB, to do so click on _Generate/switch to board_.

### Board

- After we create the board, we'll see something like that:  
  ![Schematic](../images/week08/Board1.jpg) 
- Select all of the components and move them into the work area.  
- Place them on a way that can be easier to make the connections:   
  ![Schematic](../images/week08/Board2.jpg) 
- Create the connections guided with the airwires:  
  ![Schematic](../images/week08/Board3.jpg)  
- Create a polygon around our circuit and name it "GND":  
  ![Schematic](../images/week08/Board4.jpg)  
- Click on _Rastnest_, and the polygon will be filled merging the GND connections:  
  ![Schematic](../images/week08/Board5.jpg) 
- We can resize the work area almost to the size of our circuit.  
- We export it: _Export_ > _Image_. Select monochrome and put resolution to 1500 dpi:  
  ![Schematic](../images/week08/Board6.png)  
    Now this circuit is ready to the next part, which would be to set it up to mill it.

## Download files

\- Exercise: [EaglePractice.rar](../images/week08/Assignment8.rar)  