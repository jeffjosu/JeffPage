# Project management | Create Documentation

This first week I worked on getting used to the documentation process.

For this assignment I created a repository which includes a page (the one your are reading right now).

## Softwares

\- For Control Version we used [GitLab](https://gitlab.com/) with [GitBash](https://gitforwindows.org/).  
\- For documentation structure we used Markdown.  
\- For code edition I used [Visual Studio Code](https://code.visualstudio.com/).

## GitLab

### Create an account

- Go to [GitLab](https://gitlab.com/users/sign_up).

- Register a new account, the easiest way (and the one I used) for that is _Create an account with Google_:  <br>
  ![Create account](../images/week01/GitLabRegister1.jpg)

- _Continue_ to accept the Terms of Service and Privacy Policy.  
  Account already done! Welcome to GitLab:  <br>
  ![Account created!](../images/week01/GitLabRegister03.jpg)

### Create a new project

- _New_ > _New project_ > _Create blank project_:  <br>
  ![Create new project](../images/week01/NewProject01.jpg)

- Fill in _Project name_.  
  _Project slug_ is what the GitLab instance will use as the URL path to the project and it's automatically filled with _Project name_, if you want to personalize it you can edit it.

- Write a _Project description (optional)_.

- Set the _Visibility Level_ preferably as _Public_, this will be applied to all of our [ future ] files and pages

- _Create project_:  <br>
  ![Create new project](../images/week01/NewProject002.jpg)  
  New project created:  <br>
  ![New Project Created](../images/week01/EmptyProject.jpg)

### Get page link

Once we already have our page files (most important, the `.gitlab-ci.yml`) in GitLab, GitLab we go: _Projects_ > SelectTheProject > _Settings_ > _Pages_

## GitBash

### Installation

\- [Git for Windows](https://gitforwindows.org/)

\- [Git for MacOc](https://git-scm.com/download/mac)

\- Git for Linux: `$ apt-get install git` or `“yum install git”`

When Git is downloaded, we get GitBash for command line, and GitGUI which is a more graphical version. We are going to work with GitBash.

> Note: I'm currently using Windows.

### Settings

- Open GitBash.
- Add username: `$ git config --global user.name "your_username"`
- Add email address: `git config --global user.email "your_email_address@example.com"`

Something like this:  <br>
![Git Setting](../images/week01/GitBashSetUp.jpg)

### Authentication | SSH-KeyGen

To connect our computer with GitLab, we need to add our credentials to identify ourselves.

- Open GitBash

`$ ssh-keygen -t rsa -C "your_email_address@example.com"`

- Press _Enter_ until it says "The key's randomart image is:":  <br>
  ![Git Setting](../images/week01/SSHKey1.jpg)

- Open GitLab > _Account_ > _Settings_ > _SSH Keys_ > paste there the content of the created file:  <br>
  ![Git Setting](../images/week01/SSHKey2.jpg)

### Clone a Repository

There are two main options to create our web sites: use HTML or Markdown. This time I chose Markdown, cause I rather focus my time more on creating a useful documentation, then I'll be using the Markdown [FabLab Template](https://gitlab.com/satshas/template-test/-/tree/master/docs) which already have an space for the Assignments.

- Open the folder in the PC where you are going to work

- Right CLick > _Git Bash Here_

For cloning use the command `git clone <repository path to clone>`. We can either clone it via HTTPS or SSH (the one I used)

For getting the path: Open Git repository to clone > _Clone_:  <br>
![Git Setting](../images/week01/Clone1.jpg)

Cloning via HTTPS: `git clone <https://gitlab.com/satshas/template-test.git>`

Cloning via SSH: `git clone <git@gitlab.com:satshas/template-test.git>`

Something like this:  <br>
![Git Setting](../images/week01/Clone2.jpg)

### Basic commands

After cloning, we mainly need the next commands:

-To locally create a repository with GIT: `$ git init`

-To indicate the status of the repository; modified, added and deleted files: `$ git status`

-To add to the repository ALL files and folders that are in our project: `$ git add -A`

-To commit the files that have been modified and GIT: `$ git commit -m "add_a_message"`

> Note: Afterwards the message will allow you to know exactly what modification you made in that commit.

-To upload the files to the remote repository: `$ git push -u origin master`

## Markdown

Markdown is a lightweight markup language to add formatting elements to plaintext text documents.

These are the files with the extension `*.md`

Some Basic Markdown syntax for editing your documentation:

| Element    | Description                       |
| ---------- | --------------------------------- |
| Heading    | # H1 <br> ## H2 <br> ### H3       |
| Bold       | ** bold text **                   |
| Italic     | _ italicized text _               |
| Blockquote | > blockquote                      |
| Image      | ![alt text] (../images/image.jpg) |
| Link       | [title] (https://www.example.com) |

## Code editor

[Visual Studio Code](https://code.visualstudio.com/) is a source code editor developed by Microsoft for Windows , Linux, and macOS:  <br>
![VisualStudioCode](../images/week01/Editor.jpg)

This is the Code editor that I personally like to use, includes Page preview for checking how your page is generally going to look like, and also Git commands built-in.

**Git Commands:**

To Add changes: _Source Control_ > _Views and more actions_ > _Changes_ > _Stage all Changes_.

To Commit: _Source Control_ > _Views and more actions_ > _Commit_ > _Commit all_.

To Push: _Source Control_ > _Views and more actions_ > _Push_.

![SourceControl](../images/week01/Editor2.jpg)
