# Parametric design & Laser cutting | Chess Set

This third week I worked on getting used to Parametric design and Laser engraving and cutting.

For this assignment I made a chess whit the material for reusing available in the FabLab, I chose 3mm Plexiglass.

## Softwares

\- For designing we used [Fusion 360](https://www.autodesk.com/education/edu-software/overview?sorting=featured&page=1#).  
\- For setting up the cutting in the Lab we used [RhinoCeros](https://www.rhino3d.com/).

## Parametric design

### What does it consist of?

Applicable to both 2D and 3D designs, the "Parametric design" consists in listing a sheet with all of the important parameters for our design such as length, width, thickness, patterns, diameters and more, either simple values or more complex functions, then we can use the functions instead of the number directly; it allows us to keep the key parameters in our design and change them without the need of changing the objects by ourselves, very useful when it comes to use materials with different thickness, kerf and so on:

For my chess set I applied the parametric design in the pieces, since they are made with 2 2D figures and a circular base, I needed to take in consideration the thickness of the material and the Kerf.

- In the _Sketch_ tab > _Modify_ > _Change parameters:_  <br>
  ![Parameters](../images/week03/Parameters.jpg)
  ![PiecesDesign](../images/week03/Parameters2.jpg)

### Kerf

Kerf is the width of material removed by a cutting process, in this case by the laser. In order to get strong joints, we should take in account this measurement, subtracting it in our design as an offset to the surfaces to joint.

**Measurement:**

Since the kerf, in this case, is given in millimeters, in order to get a more accurate measurement we can cut some small pieces from the same sheet we are going to use for our project.

- I cut 4 15x15mm pieces:  <br>
  ![KerfMeasurement](../images/week03/Kerf.jpg)

    - Some calculation:
        - Amount of pieces: N = 4
        - Designed total length: DTL = 4x15 = 60mm
        - Real total length: RTL = 58.4mm
        - Kerf = (DTL-RTL) / N = (60-58.4)mm/4 = 1.6mm/4 = 0.4mm

> Note: We should measure the kerf every time we use different a material, different thickness or different laser cutter.

## Laser cutting process

It's worth to mention that there are two CO2 laser cutters at the FabLab:

1. Epilog Zing 30 Watts ("The small one"):  <br>
  ![EpilogZing](../images/week03/SmallOne.jpg)

2. Epilog Fusion 60 Watts ("The big one"):  <br>
  ![EpilogFusion](../images/week03/BigOne.jpg)

> Note: I used the Small one.

### Processes & parameters

There are a couple of processes we can make in a laser cutter, besides cutting:  
\- Cutting.  
\- Engraving.  
\- Marking.

**Parameters:**

There are some parameters we need to setup before printing, all of the depend on what process we are going to make, and on the material we use.

\- _Speed_, given in percentage, describes the movement of the laser head. Fast speeds lead to short exposure times, slow speeds lead to long exposure times:  
 \* High speeds for engraving.  
 \* Low speeds for cutting.

\- _Power_, given in percentage, describes the output power of the laser. 100% is maximum power:  
 \* High power for cutting.  
 \* Low power for engraving.

\- _Frequency_ specifies the number of laser pulses per second and is used only for cutting:  
 \* High frequency = high heat.  
 \* Low frequency = low heat.

> Note: Those are the general settings, they could change depending on you material or the finish you want.

### Steps

#### On the computer

- Open the \*.dxf file.  <br>
  ![OpenFile](../images/week03/PiecesRhino.jpg)
- If working on different process, setup the layers for each one.
- Setup the printing:
    - Sheet size.
    - Set the draw position in the sheet.
    - Setup speed, frequency and power for every layer we use.  
      \- For cutting I used:
      ![CuttingValues](../images/week03/CutValues.jpg)

- Send the printing to the laser cutter when this one is ready already.

#### On the laser cutter

- Turn on the laser cutter and wait for it to initialize.
- Put the material to cut on the bed and make sure it's flat:  <br>
  ![PieceOnBed](../images/week03/PieceOnBed.jpg)  

_User control interface:_  <br>
  ![Control](../images/week03/Control.jpg)

- Turn on the laser bean:  
    - Press Button 7.

- Change home position:  
    - Press Button 8.
    - Move the laser head with your hand.
    - Press PLAY.

- Focus the laser:  
    - Press Button 5.
    - Move the bed using Buttons 3 and 4.
    - Use the focus tool, when should barely touch the sheet.  <br>
      ![FocusTool](../images/week03/FocusTool.jpg)

      > Note: The position of the bed doesn't need to be kept.

    > Remember: When done, lift the tool back to its place.

- Turn on the compressor:  <br>
  ![EngravingValues](../images/week03/Compressor.jpg)
- To select your file:  
  At this point send the file from the computer to the laser cutter.  
    - Press 6.
    - Look for your file using Buttons 3 and 4.
    - Press PLAY.

    > Advice: Once your piece is done, wait some seconds in order to make sure the compressor sucked all of the emissions which can be more or less dangerous depending on the material we use; when finished, you can already turn off the compressor and take out your piece.

## Making

\- I made the foundation, squares, numbers and letters directly in RhinoCeros:  <br>
![NumbersAndLetters](../images/week03/NumbersAndLetters.jpg)  
\- Cut 34x34 cm foundation:  <br>
![CutFoundation](../images/week03/Pieces.jpg)  
\- Cut squares and numbers:  <br>
![CutNumbersAndLetters](../images/week03/Pieces2.jpg)  
\- Gluing squares and numbers on the foundation:  <br>
![GluingNumbersAndLetters](../images/week03/Pieces3.jpg)  
\- Cut pieces:  <br>
![CutPieces](../images/week03/Pieces4.jpg)

\- Finally, the Chess Set:  <br>
![CutPieces](../images/week03/Finish1.jpg)
![CutPieces](../images/week03/Finish2.jpg)
![CutPieces](../images/week03/Finish3.jpg)

\-Also played some rounds with a colleagues already:  <br>
![CutPieces](../images/week03/Finish4.jpg)

\- Since I also had to do some Engraving for the assignment, I made a double sided piece:  <br>
![EngravingPiece](../images/week03/EngravePiece.jpg)  
\* For engraving I used these parameters:  <br>
![EngravingPiece](../images/week03/EngraveValues.jpg)

## Experience and advices

\- Make small tests before cutting the official pieces to make sure your parameters are the right ones.

\- Measure the Kerf after having your official parameters.

\- Make sure to cut the same material with which you measured the kerf, because every material has different properties so different behavior with the laser, giving different kerf sizes.

\-I didn't realized that I was using two different plastic materials, so I really make the measurement in one material and used the other one, then I got some pieces nicer then others.

## Download files

\- Parametric design - Chess pieces: [ChessPieces.f3d](../images/week03/ChessPiece.f3d)  
\- RhinoCeros file - Numbers and letters: [NumbersAndLetters.3dm](../images/week03/NumbersAndLettersRhino.3dm)
