# 3D Print | Hook

This fourth week I worked on getting used to 3D printing.

For this assignment I printed the hook I modeled in the [second week](https://jeffjosu.gitlab.io/JeffPage/assignments/week02/).

For some inspiration or 3D models already designed, you can check these pages:  
\- [TraceParts](https://www.traceparts.com/en)  
\- [GrabCAD](https://grabcad.com/)  
\- [Thingiverse](https://www.thingiverse.com/)

## Softwares and hardware

\- For slicing we used [Cura](https://ultimaker.com/software).  
\- The 3D printer I used was [Ultimaker S5](https://ultimaker.com/3d-printers/ultimaker-s5), with a AA 0.4 nozzle.  
\- The material I used for printing was generic PLA.

## 3D print technologies

| Type              | Technologies                                                                                                                                                                                                 |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Extrusion         | Fused filament fabrication (FFF)                                                                                                                                                                             |
| Light polymerized | Stereolithography (SLA)<br>Digital Light Processing (DLP)                                                                                                                                                    |
| Powder bed        | Powder bed and inkjet head (3DP)<br>Electron-beam melting (EBM)<br>Selective laser melting (SLM)<br>Selective heat sintering (SHS)<br>Selective laser sintering (SLS)<br>Direct metal laser sintering (DMLS) |
| Laminated         | Laminated object manufacturing (LOM)                                                                                                                                                                         |
| Powder fed        | Directed Energy Deposition (DED)                                                                                                                                                                             |
| Wire              | Electron beam freeform fabrication (EBF)                                                                                                                                                                     |

> The technology we learned in the lecture was FFF.

## Parameters

Before starting with the process, we got to know how the parameters work, there are a lot of parameters to setup our printings, but the main ones we need to know how to use (in the FFF technology) are:

\- **Layer height**: as it sounds, it's the exact height of each layer of material extruded, is given in mm and affects the final printing resolution.  
 \* When working with pieces that have vertical curves such as spheres (or in general need good vertical definition), then thinner layers are preferred.  
 \* When working with pieces that are basically vertical extrusions such as cylinders and cubes (or in general doesn't need good vertical definition), then thicker layers are Ok.

> To consider:  
>  \- Thicker layer = much shorter printing time.  
>  \- Thinner layer = much longer printing time.

\- **_Shell_**: refers to the thickness of the shell of piece, given in mm and affects the final strength of the piece.  
 \* When working with pieces that are part of a mechanism and/or need to be strong, then thicker shells are preferred.  
 \* When working with didactic pieces, tests, decoration (or in general doesn't need to be strong), then thinner shells are OK.

> To consider:  
>  \- Thicker shells = longer printing time.  
>  \- Thinner shells = shorter printing time.

\- **_Infill_**: as it sounds, it refers to how solid is internally the piece going to be, is given in percentage and affects the final strength of the piece.  
\* When working with pieces that are part of a mechanism and/or need to be strong, then higher infill percentages are preferred.  
 \* When working with didactic pieces, tests, decoration (or in general doesn't need to be strong), then lower infill percentages are OK.

> To consider: When need stronger pieces, increasing the infill is preferred against increasing the shell thickness due to the printer make it faster.  
> \- Lower infill = a little shorter printing time.  
> \- Higher infill = a little longer printing time.

\- **_Support_**: is a structure used when the model has an overhang or a bridge which is not supported by anything below, is given in percentage.

> To consider: The effect on the printing time is going to depend on how much of them we need, but always prefer to print the piece in a position that needs less position.

\- **_Plate adhesion_**: is a construction that adds width to the first printing layer in order to get a better adhesion to the plate.  
 \*Usually thin constructions are enough.

> To consider: Using it practically doesn't affect the printing time.

## Printing process

### Getting STL file

For slicing our model, we need to use a file which the slicer can read; the most used for it is the \*.stl.

For getting the \*.stl file from the Fusion 360, we have two options:

1. Open the _Bodies_ > Right click on the desired body > _Save as STL_:  <br>
  ![SaveSTL](../images/week04/SaveSTL.jpg)

2. _File_ > _Export..._ > in _Type_ select _\*.stl Files_ > _Export_:  <br>
![SaveSTL](../images/week04/SaveSTL2.jpg)

### Slicer

ALso known as "slicing software" is used for the the conversion from a model in STL format to printer commands in g-code format that tell the 3D printer the movements.

It is to worth mention that since this is only a didactic printing whose intention is solely for learning, the settings are not intended for a real application.

**Structure and settings:**

- The _Prepare_ tab allow us to quickly setup our printing:
  ![PrepareTab](../images/week04/Cura1.jpg)

    - Top Left:
        - _Open file_:  <br>
          ![OpenFile](../images/week04/Cura2.jpg)

    - Top:
        - _Printer:_ Ultimaker S5  <br>
          ![SelectPrinter](../images/week04/Cura3.jpg)
        - _Material_ and _Print core:_ Generic PLA, AA 0.4  <br>
          ![MaterialAndNozzle](../images/week04/Cura4.jpg)

          > There are two spaces due to it's possible to print with two materials, but for this practice we are using only one.
    - _Print settings_ (also known as _parameters_):  <br>
        ![PrintSettings](../images/week04/Cura5.jpg)
        - _Quality_ > _Layer Height_: 0.1mm (Cause I want my piece to look detailed enough, since is made mainly of curves, a thicker layer height would cause it lo lose its shape).  
        - _Shell_ > _Wall thickness:_ 1mm (by default, I don't need it to be stronger than that).  
        - _Infill_ > _Infill density:_ 15% (A low density cause I don't need it to be strong, only want to prevent the top from failing).  
        - _Support:_ Activated, at 15% (Need to activate it due to most of my piece doesn't touch the bed. It only needs to hold the printing, low density is OK).  
        - _Build Plate adhesion:_ Brim at 4mm (I only needed to make sure my piece doesn't come out in the process, taking in consideration that mostly only the supports are in contact with the bed).  

    - Left, the most important are:
        - _Move_ model:  <br>
          ![MoveModel](../images/week04/Cura6.jpg)
        - _Scale_ model:  <br>
          ![ScaleModel](../images/week04/Cura7.jpg)
        - _Rotate_ model:  <br>
          ![RotateModel](../images/week04/Cura8.jpg)

    - Bottom left:  <br>
      ![BottomLeft](../images/week04/Cura9.jpg)
        - List of models.
        - Printing file name.
        - Printing volume.
        - Views.

    - Bottom right:  <br>
      ![BottomRight](../images/week04/Cura10.jpg)
        - Printing time.
        - Material: weight and filament length for this print.
        - _Preview_ and _Save file_.

- The _Preview_ tab allows us to watch the printing layer by layer, check the supports, infill and plate adhesion structure:  <br>
  ![PreviewTab](../images/week04/Cura11.jpg)

### 3D printer

- Insert USB stick:  <br>
  ![InsertUSB](../images/week04/Printing1.jpg)

- _Select from USB_:  <br>
  ![SelectFromUSB](../images/week04/Printing2.jpg)

- Select file to print:  <br>
  ![SelectFIle](../images/week04/Printing3.jpg)

- In this window it shows a preview like the one we can watch in the _Prepare_ tab on Ultimaker Cura (without supporting structures), and also indicates the printing time, material and filament length for this print. _Start print_:  <br>
  ![StartPrint](../images/week04/Printing4.jpg)

- Preparing to print. During this time (about 6mins in this printer model) it heats up the plate and the nozzles, and calibrates the plate level:  <br>
  ![Preparing](../images/week04/Printing5.jpg)

- Printing:  <br>
  ![Printing](../images/week04/Printing6.jpg)

- Finished printing:  <br>
  ![FinishedPrinting](../images/week04/Printing7.jpg)

> Recommendation: after the print finishes wait a couple of minutes for the plate to cool down and remove the piece easier.

- _Confirm Removal_:  <br>
  ![ConfirmRemoval](../images/week04/Printing8.jpg)

### Post process

- Last step is to remove the supporting structures and file the resulting rustic parts:  <br>
  ![FinishedPrinting](../images/week04/Printing9.jpg)  
  ![FinishedPrinting](../images/week04/Printing10.jpg)

## Download files

\- STL File: [Hook.stl](../images/week04/Hook.stl)  
\- Printing file: [UMS5_Hook.ufp](../images/week04/UMS5_Hook.ufp)
