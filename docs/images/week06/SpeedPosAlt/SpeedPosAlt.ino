#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
Adafruit_MPU6050 mpu;
// I used Pi value in calculations
#define PI 3.1415926535

// declare yaw/pitch/roll auxiliar variables
float actyaw = 0, actpitch = 0, actroll = 0;
// declare auxiliar variables for accel offsets
float offsetax = 0, offsetay = 0, offsetaz = 0;
// declare auxiliar variables for gyro offsets
float offsetgx = 0, offsetgy = 0, offsetgz = 0;

// declare variables for sonar sensor
#define echoPin 3
#define trigPin 4
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement

void setup(void) {
  Serial.begin(115200);
  while (!Serial)
  delay(100); // will pause until serial console is open

  // Initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {delay(10);}
  }
  Serial.println("MPU6050 Found!");

  // wait for ready
  Serial.println(F("\nPress Enter to begin: "));
  while (Serial.available() && Serial.read()); // empty buffer
  while (!Serial.available());                 // wait for data
  while (Serial.available() && Serial.read()); // empty buffer again  

  // sensor parameters setup
  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.println("");
  delay(100);

  // Get new sensor events with the readings
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  offsetax = a.acceleration.x;
  offsetay = a.acceleration.y;
  offsetaz = a.acceleration.z;

  offsetgx = g.gyro.x;
  offsetgy = g.gyro.y;
  offsetgz = g.gyro.z;

  // Ultrasonic sensor pins setup
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  // Get new sensor events with the readings
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);
  
  // Calculate yaw/pitch/roll from gyroscope values, and print
  // From the gyroscope we get angular velocity in 'rad/s'
  // First I converted 'rad/s' to 'deg/s', then multiplied it by Time to get 'deg'
  float yaw = ((g.gyro.x - offsetgx)*180/PI) * 0.1;
  float pitch = ((g.gyro.y - offsetgy)*180/PI)  * 0.1;
  float roll = ((g.gyro.z - offsetgz)*180/PI) * 0.1;
  actyaw = actyaw + yaw;
  actpitch = actpitch + pitch;
  actroll = actroll + roll;
  
  Serial.print("Yaw: ");
  Serial.print(actyaw, 1);
  Serial.print("º\tPitch: ");
  Serial.print(actpitch, 1);
  Serial.print("º\tRoll: ");
  Serial.print(actroll, 1);
  Serial.println("º");

  // Calculate velocity from accelerometer values
  // From the accelerometer we get acceleration in 'm/s^2'
  // First I multiplied it by Time to get velocity in 'm/s'
  float velx = (a.acceleration.z - offsetaz)*0.1;
  float vely = (a.acceleration.y - offsetay)*0.1;
  
  // Calculate total horizontal speed, and print
  // First I calculated horizontal total speed with velocity in X and Y
  // I multiplied it by 100 for getting 'cm/s'
  float hspeed = sqrt((velx*velx)+(vely*vely));
  Serial.print("Hor speed: ");
  Serial.print(hspeed*100, 2); 
  Serial.println("cm/s");

  // basic loop for HC-SR04 UtraSonic Distance sensor
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = (duration * 0.034 / 2)-1; // Speed of sound wave divided by 2 (go and back)
  // Displays the distance on the Serial Monitor
  Serial.print("Altitude: ");
  Serial.print(distance);
  Serial.println(" cm\n");
  
  delay(100);
}
