//Libraries for Communication part (nRF24)
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); //Create radio object with and CE & CSN pins
const byte address[6] = "00111"; //Transmitter communication address

void setup() {
  radio.begin(); //Initialize radio object
  radio.setPALevel(RF24_PA_HIGH); //Set radio range
  radio.openWritingPipe(address); //Set address to receiver
  radio.stopListening(); //Set module as transmitter
}

void loop() {
  int Values[3]; //

  //Read values from Joysticks and convert them to percentage
  Values[0] = map(analogRead(A0),0,1023,100,-100); //Forward and Backward movement from Joystick_1
  Values[1] = map(analogRead(A1),0,1023,-100,100); //Left and Right movement from Joystick_1
  Values[2] = map(analogRead(A3),0,1023,-100,100); //Rotation movement from Joystick_2

  radio.write(&Values, sizeof(Values)); //Send to Receiver an array that containing the Values

  delay(100); //Send values every 0.1 seconds
}
