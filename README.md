## Welcome to My Fundamentals on Digital Fabrication - Documentation

This site is to document my activities during the _Fundamentals of digital fabrication_ course, offered by the [FabLab Kamp-Lintfort](https://fablab.hochschule-rhein-waal.de/) at the Hochschule Rhein Waal and executed by Mr. Daniele Ingrassia during the Winter Semester 2020/21

I'm taking this course because I want to improve my digital fabrication skills.

## Page link:

https://jeffjosu.gitlab.io/FoDF2020/

## Template used:

Link to the original template:
https://gitlab.fabcloud.org/fibasile/fabacademy-student-template
